package ru.b3nder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomLoader extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) {
        byte[] bytes = loadClassData(name);
        return defineClass(name, bytes, 0, bytes.length);
    }

    private byte[] loadClassData(String className) {
        InputStream inputStream = getClass().getClassLoader()
                .getResourceAsStream(className.replace('.', '/') + ".class");

        ByteArrayOutputStream byteSt = new ByteArrayOutputStream();
        int len;

        try {
            while ((len = inputStream.read()) != - 1) {
                byteSt.write(len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteSt.toByteArray();
    }
}
