package ru.b3nder;

import java.lang.reflect.Method;

public class MainClass {
    public static void main(String[] args) throws Exception {
        CustomLoader loader = new CustomLoader();

        Class<?> clazz = loader.findClass("ru.b3nder.Test");
        Object object = clazz.getDeclaredConstructor().newInstance();

        Method method = object.getClass().getMethod("print");
        method.invoke(object);
    }
}
